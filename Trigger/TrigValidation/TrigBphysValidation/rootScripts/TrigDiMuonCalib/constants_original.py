# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration


##  Inner station ********************************

##Barrel
InnerBarrel_c0=0.002
InnerBarrel_c1=508.4

##Endcap
##bin 1 : fabs(etaId)>1.6
InnerEndcap_etaBin1_c0=0.0007
InnerEndcap_etaBin1_c1=303.2

##bin 2 : fabs(etaId)>1.3 && fabs(etaId)<=1.6
InnerEndcap_etaBin2_c0=0.001
InnerEndcap_etaBin2_c1=402.1

##bin 3 : fabs(etaId)<=1.3
InnerEndcap_etaBin3_c0=0.0005
InnerEndcap_etaBin3_c1=478.6

##  Middle station ********************************
## Barrel
MiddleBarrel_c0=0.0015
MiddleBarrel_c1=57.4
MiddleBarrel_c2=699121.5

## Endcap
## etaBin1 : fabs(etaId)>2.
MiddleEndcap_etaBin1_phiBin1_c0=0.002
MiddleEndcap_etaBin1_phiBin1_c1=463.8
MiddleEndcap_etaBin1_phiBin1_c2=404225.8

MiddleEndcap_etaBin1_phiBin2_c0=0.0016
MiddleEndcap_etaBin1_phiBin2_c1=407.6
MiddleEndcap_etaBin1_phiBin2_c2=379832.4

MiddleEndcap_etaBin1_phiBin3_c0=0.0017
MiddleEndcap_etaBin1_phiBin3_c1=361.6
MiddleEndcap_etaBin1_phiBin3_c2=447396.5

## etaBin2 : fabs(etaId)>1.65 && fabs(etaId)<=2.
MiddleEndcap_etaBin2_phiBin1_c0=0.0023
MiddleEndcap_etaBin2_phiBin1_c1=349.7
MiddleEndcap_etaBin2_phiBin1_c2=590774.7

MiddleEndcap_etaBin2_phiBin2_c0=0.002
MiddleEndcap_etaBin2_phiBin2_c1=259.1
MiddleEndcap_etaBin2_phiBin2_c2=565283.6

MiddleEndcap_etaBin2_phiBin3_c0=0.0016
MiddleEndcap_etaBin2_phiBin3_c1=210.1
MiddleEndcap_etaBin2_phiBin3_c2=580269.5

## etaBin3 : fabs(etaId)>1.35 && fabs(etaId)<=1.65
MiddleEndcap_etaBin3_phiBin1_c0=0.0018
MiddleEndcap_etaBin3_phiBin1_c1=212.3
MiddleEndcap_etaBin3_phiBin1_c2=435331.3

MiddleEndcap_etaBin3_phiBin2_c0=0.00084
MiddleEndcap_etaBin3_phiBin2_c1=155.8
MiddleEndcap_etaBin3_phiBin2_c2=379710.6

MiddleEndcap_etaBin3_phiBin3_c0=0.0003
MiddleEndcap_etaBin3_phiBin3_c1=23.7
MiddleEndcap_etaBin3_phiBin3_c2=521576.8

## etaBin4 : fabs(etaId)>1.2 && fabs(etaId)<=1.35
MiddleEndcap_etaBin4_phiBin1_c0=0.0011
MiddleEndcap_etaBin4_phiBin1_c1=260.
MiddleEndcap_etaBin4_phiBin1_c2=1049470.8

MiddleEndcap_etaBin4_phiBin2_c0=0.003
MiddleEndcap_etaBin4_phiBin2_c1=178.9
MiddleEndcap_etaBin4_phiBin2_c2=1290924.

MiddleEndcap_etaBin4_phiBin3_c0=0.002
MiddleEndcap_etaBin4_phiBin3_c1=86.2
MiddleEndcap_etaBin4_phiBin3_c2=1668021.3

## etaBin5 : fabs(etaId)<=1.2
MiddleEndcap_etaBin5_phiBin1_c0=0.0032
MiddleEndcap_etaBin5_phiBin1_c1=269.9
MiddleEndcap_etaBin5_phiBin1_c2=2258893.9

MiddleEndcap_etaBin5_phiBin2_c0=0.005
MiddleEndcap_etaBin5_phiBin2_c1=173.4
MiddleEndcap_etaBin5_phiBin2_c2=2848828.3

MiddleEndcap_etaBin5_phiBin3_c0=0.0048
MiddleEndcap_etaBin5_phiBin3_c1=153.1
MiddleEndcap_etaBin5_phiBin3_c2=3344228.2

MiddleEndcap_c0=0.00066
MiddleEndcap_c1=179.6
