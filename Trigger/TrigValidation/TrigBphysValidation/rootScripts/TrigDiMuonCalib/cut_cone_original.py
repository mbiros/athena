# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

## Hit-track matching cut-cone constants

Cone_Endcap_mdt_inner_dEta_c0=44.638
Cone_Endcap_mdt_inner_dEta_c1=0.9109

Cone_Endcap_mdt_middle_dEta_c0=131.57
Cone_Endcap_mdt_middle_dEta_c1=0.978

Cone_Barrel_mdt_inner_dEta_c0=44.638
Cone_Barrel_mdt_inner_dEta_c1=0.9109

Cone_Barrel_mdt_middle_dEta_c0=42.654
Cone_Barrel_mdt_middle_dEta_c1=0.8662

Cone_rpc_middle_dEta_c0=42.654
Cone_rpc_middle_dEta_c1=0.8662

Cone_rpc_middle_dPhi_c0=241.76
Cone_rpc_middle_dPhi_c1=1.038

Cone_tgc_wire_inner_dEta_c0=9.6
Cone_tgc_wire_inner_dEta_c1=0.7472

Cone_tgc_wire_middle_dEta_c0=131.57
Cone_tgc_wire_middle_dEta_c1=0.978

Cone_tgc_strip_inner_dPhi_c0=24.11
Cone_tgc_strip_inner_dPhi_c1=0.85588

Cone_tgc_strip_middle_dPhi_c0=1264.3
Cone_tgc_strip_middle_dPhi_c1=1.1423
Cone_tgc_strip_middle2_dPhi_c0=82.017
Cone_tgc_strip_middle2_dPhi_c1=0.9231
