#!/bin/python

# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
##-----------------------------------------------------------
##
## runAnalyzeAllHitMatching.py
##
## Configuration script for TrigDiMuonCalib
## Creates ROOT macro with desired configuration
##
## author: Daniel Scheirich <scheiric@cern.ch>
## Part of TrigDiMuonCalib in TrigBphysValidation package
##
##-----------------------------------------------------------
import string, os

##-----------------------------------------------------------
## Basic settings
##-----------------------------------------------------------

outputLevel = "INFO"                  # output level: "DEBUG", "INFO", "WARNING", "ERROR"
execute     = True                    # execute macro
chain       = "L2_MU4_DiMu_FS"        # chain to be analyzed

##-----------------------------------------------------------
## More settings
##-----------------------------------------------------------
## add flags to the settings string to activate features

settings     = ''
settings    += ' DoMuonMatching '
#settings    += ' DoJpsiMatching '
settings    += ' DRCut=0.01 '
settings    += ' ConstantsFile=\\"constants.py\\" '
#settings    += ' MaxEvt=50000 '
#settings    += ' NoExtrapolation '
settings    += ' DrawGaus '
settings    += ' SaveCutCone '


##-----------------------------------------------------------
## Input files
##-----------------------------------------------------------
## You can either specify list of files explicitly using inFileNames
## or you can load entire directory using dirPath vatiable

## list of input files created by TriggerJpsiAnalysis algorithm
inFileNames  = [ ]
inFileNames += [ "/net/s3_datac/scheiric/data/output_TrigDiMuonCalib/2010-06-22/rootfiles/user.scheiric.13.data10_7TeV_MuonswBeam.00155112.TrigDiMuonCalib.bphys.root" ]

## directory containing root files created by TriggerJpsiAnalysis algorithm
#dirPath       = "/net/s3_datac/scheiric/data/output_TrigDiMuonCalib/2010-06-21/rootfiles"

##-----------------------------------------
## Main script -- DO NOT EDIT !
##-----------------------------------------
## Jpsi selection cuts

import jpsiCuts

## create cuts string

cutStr = ""

for cut in jpsiCuts.cuts:
  cutStr += cut + " "


## generate root macro

macro   = ""
macro   += "// Generated by runAnalyzeAllHitMatching.py\n"
macro   += "#include <vector>\n"
macro   += "#include <string>\n"
macro   += "#include \"Log.h\"\n"
macro   += "void runAnalyzeAllHitMatching() {\n"
macro   += "  gSystem->Load(\"TrigDiMuonRootCalib_cpp.so\");\n"
if('dirPath' in dir()):
  macro   += "  TrigDiMuonRootCalib analysis(" + outputLevel + ", \"" + dirPath + "\");\n"

else:
  macro   += "  std::vector<std::string> files;\n"
  for file in inFileNames :
    macro += "  files.push_back( \"" + file + "\" );\n"
  macro   += "  TrigDiMuonRootCalib analysis(" + outputLevel + ", files);\n"

macro   += "  analysis.analyzeAllHitMatching(\""+ chain + "\", \"" + settings +"\", \""+ cutStr +"\");\n"
macro   += "}\n"

## create macro
f = open("runAnalyzeAllHitMatching.C","w")
f.write(macro)
f.close()

## execute root macro
if(execute) :
  os.system( "root -l runAnalyzeAllHitMatching.C" )

## end of script



