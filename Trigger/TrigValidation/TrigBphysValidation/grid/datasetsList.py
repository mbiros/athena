# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration


datasets = [ ]

##----------------------------------------
## 2010 May reprocessing + more datasets
##----------------------------------------

### Period B (reprocessed)
#datasets += [ "data10_7TeV.periodB.physics_MuonswBeam.PhysCont.AOD.repro04_v01/" ]

## Period C (Tier-0 reconstruction)
#datasets += [ "data10_7TeV.periodC.physics_MuonswBeam.PhysCont.AOD.t0pro04_v01/" ]

## Period D
#datasets += [ "data10_7TeV.periodD.physics_MuonswBeam.PhysCont.AOD.t0pro04_v01/" ]

## Period E
#datasets += [ "data10_7TeV.periodE.physics_Muons.PhysCont.AOD.t0pro04_v01/" ]

## Period F
datasets += [ "data10_7TeV.periodF.physics_Muons.PhysCont.AOD.t0pro04_v01/" ]

## Period G
#datasets += [ "data10_7TeV.periodG1.physics_Muons.PhysCont.AOD.t0pro04_v01/" ]
#datasets += [ "data10_7TeV.periodG2.physics_Muons.PhysCont.AOD.t0pro04_v01/" ]
#datasets += [ "data10_7TeV.periodG3.physics_Muons.PhysCont.AOD.t0pro04_v01/" ]
#datasets += [ "data10_7TeV.periodG4.physics_Muons.PhysCont.AOD.t0pro04_v01/" ]
#datasets += [ "data10_7TeV.periodG5.physics_Muons.PhysCont.AOD.t0pro04_v01/" ]
